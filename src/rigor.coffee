net = require 'net'
_ = require 'underscore'
readline = require('readline');
app = require('./app')
io = app.io

class Rigor
  init: () ->
    @workers = []
    @queue = []
    self = this
    net.createServer( (sock) ->
      worker = {
        socket: sock
        job: null
        started: false
        connected: true
      }

      sock.setKeepAlive(true)

      wlog = app.log.child {address: sock.remoteAddress, port: sock.remotePort}
      wlog.info {num_workers: self.workers.length}, 'socket connected to rigor manager'

      rd = readline.createInterface {
        input: sock,
        output: process.stdout,
        terminal: false
      }

      rd.on 'line', (data) ->
        if data == 'start worker: 893heuwdnjsu3w8djsdaiudi3uwehd'
          @workers.push worker
          wlog.info {num_workers: self.workers.length}, 'worker authenticated'
          app.log.trace @workers
          socket.emit('status', workers)
          self.dequeue_to worker
          worker.started = true
          worker.connected = true
        else if data != 'start'
          try
            res = JSON.parse data
            worker.connected = true
            worker.callback(res.type, res.data)
            if res.type == 'progress'
              wlog.debug res, 'sent progress to client'
            else if res.type == 'answer'
              wlog.trace res, 'sent answer to client'
              worker.job = null
              io.emit('status', @workers)
              self.dequeue_to worker
            else if res.type == 'error'
              wlog.error res, 'error while running rigor'
              worker.job = null
              self.dequeue_to worker
            else if res.type == 'warn'
              wlog.warn res, 'warning while running rigor'
            else wlog.warn res, 'unknown response type'

          catch err
            wlog.warn {data: data}, 'socket sent unparsable json data to rigor manager'

      sock.on 'close', (data) ->
        @workers = _.without(@workers, worker)
        if worker.started
          app.log.info {address: sock.remoteAddress, port: sock.remotePort, num_workers: self.workers.length}, 'worker disconnected'
        else app.log.warn {address: sock.remoteAddress, port: sock.remotePort, num_workers: self.workers.length}, 'socket disconnected from rigor manager without authenticating as a worker'
    )

  dequeue_to: (worker) ->
    job = @queue.pop()
    app.log.trace @queue
    if job
      worker.job = job[0]
      worker.callback = job[1]
      worker.job.type = 'run'
      try
        req = JSON.stringify(worker.job)
      catch e
        app.log.error {address: worker.remoteAddress, port: worker.remotePort}, err.message
        return
      try 
        worker.socket.write req + '\n'
        io.emit('status', @workers)
        worker.callback 'progress', 'Job is running\n'
      catch err
        @queue.push job
        app.log.error {address: worker.remoteAddress, port: worker.remotePort}, err.message

  stop_workers: () ->
    for worker in @workers
      try 
        worker.socket.write '{"type": "exit"}\n'
        app.log.info 'kill'
      catch err
        app.log.error err, 'could not stop worker'
    io.emit('status', @workers)

  run: (job, callback) ->
    # @requests[job.id] = callback
    @queue.push([job, callback])
    app.log.info job, 'queued job'
    app.log.trace @queue
    app.log.trace @workers
    callback('progress', 'Job has been queued\n')
    for worker in @workers
      if not worker.job
        @dequeue_to worker
        break

  options: {
    'superpixels_method': { 
      'name': 'Super-pixels Method', 'choices': {'structedges':'Struct Edges', 'watershed': 'Watershed'}, 'type': 'choice'
    }
  }

module.exports = new Rigor()
