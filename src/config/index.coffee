#### Config file
# Sets application config parameters depending on `env` name
exports.setEnvironment = (env) ->
  switch(env)
    when "development"
      exports.DEBUG_LOG = true
      exports.DEBUG_WARN = true
      exports.DEBUG_ERROR = true
      exports.DEBUG_CLIENT = true
      exports.DB_HOST = 'mongodb://ahumayun:bBXuZy1o0yk0@ds013966.mlab.com:13966/heroku_6s5z47gk'

    when "testing"
      exports.DEBUG_LOG = true
      exports.DEBUG_WARN = true
      exports.DEBUG_ERROR = true
      exports.DEBUG_CLIENT = true

    when "production"
      exports.DEBUG_LOG = true
      exports.DEBUG_WARN = true
      exports.DEBUG_ERROR = true
      exports.DEBUG_CLIENT = true
      exports.DB_HOST = 'mongodb://ahumayun:bBXuZy1o0yk0@ds013966.mlab.com:13966/heroku_6s5z47gk'

    else
      throw "environment #{env} not found"
