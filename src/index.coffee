express = require 'express'
stylus = require 'stylus'
assets = require 'connect-assets'
session = require('express-session')
cookieParser = require('cookie-parser')
bodyParser = require("body-parser")
watch = require('watch')
globals = require './app'

#### Basic application initialization

# Define Port & Environment
port = process.env.PORT or process.env.VMC_APP_PORT or 80
env = process.env.NODE_ENV or "development"

# Create app instance.
app = express()
http = require('http').Server(app)
http.port = port

# initialize globals
globals.init(env, http)

# initialize and run worker manager
rigor = require('./rigor')
rigor.init().listen(5000, '127.0.0.1')
globals.log.info({host: '127.0.0.1', port: 5656}, 'rigor manager started and listening')

process.on 'SIGUSR2', ->
  globals.log.fatal 'server changed'
  # process.exit()
  # gracefulShutdown ->
    
  #   process.kill(process.pid, 'SIGUSR2')

# catch all uncaught exceptions
process.on 'uncaughtException', (err) ->
	globals.log.fatal err, 'uncaught exception occurred. server restarting'
	process.exit()

# release workers on exit
process.on 'exit', () ->
	globals.log.fatal 'server restarting'
	rigor.stop_workers()

#### View initialization
# Add Connect Assets.
app.use assets {
	paths: ["assets/js", "assets/css", "assets/libs"]
}

# Set the public folder as static assets.
app.use express.static(process.cwd() + '/public')

# watch for css changes
watch.createMonitor './assets/css/', (monitor) ->
	monitor.on "changed", (f, curr, prev) ->
		name = f.substring(11, f.length-5)
		globals.log.info 'css file changed: ' + name
		css('app')
		io.emit 'css', name

# watch for source changes
watch.createMonitor './.app/', (monitor) ->
	monitor.on "changed", (f, curr, prev) ->
		globals.log.info 'source file changed: ' + f
		process.exit()


# # Express Session
# console.globals.log "setting session/cookie"
# app.use cookieParser()
# app.use session(
#   secret: "keyboard cat"
#   key: "sid"
#   cookie:
#     secure: true
# )

# Set View Engine.
app.set 'view engine', 'jade'

# [Body parser middleware](http://www.senchalabs.org/connect/middleware-bodyParser.html) parses JSON or XML bodies into `req.body` object
app.use bodyParser()

#### Finalization
# Initialize routes
routes = require './routes'
routes(app)

# Export application object
module.exports = http
module.exports.log = globals.log
