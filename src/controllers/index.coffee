app = require '../app'
rigor = require('../rigor')
io = app.io
db = app.db

module.exports.index = (req, res) ->
  res.render 'index', {'options': rigor.options}
  app.log.info {'ip': req.headers['x-forwarded-for']}, 'served app'

io.on 'connection', (socket) ->
  soclog = app.log.child {id: socket.id} # log with userid
  soclog.info {ip: socket.handshake}, 'client connected'
  socket.emit('status', rigor.workers)

  socket.on 'load', (req) ->
    soclog = app.log.child {id: socket.id}
    try
      soclog.trace {req: req.id}, 'image url request'
      db.images.find(req).toArray (err, docs) ->
        try
          if err 
            soclog.fatal {req: req.id}, 'error connecting to database'
          else if docs.length
            socket.emit('load', docs[0].url)
            soclog.trace {req: req.id, url: docs[0].url}, 'image url sent'
        catch err
          soclog.error {req: req.id}, 'error getting url'

  socket.on 'run', (job) ->
    soclog = app.log.child {id: socket.id}
    try 
      soclog.info job, 'segmentation request'
      job.id = socket.id
      rigor.run job, (type, res) -> 
          socket.emit(type, res)
    catch err
      soclog.error err, 'error queing job'

  socket.on 'disconnect', () ->
    soclog = app.log.child {id: socket.id}
    soclog.info 'client disconnected'