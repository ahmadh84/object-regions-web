bunyan = require('bunyan')
MongoClient = require('mongodb').MongoClient
io = require('socket.io')
config = require "./config"

exports.init = (env, server) ->

	## initialize globals
	# initialize logger
	log_queue = []
	log_stream = {
		write : (obj) ->
			log_queue.push(obj)
	}

	log = exports.log = bunyan.createLogger {
		name:'segs-demo'
		streams: [ {
				level:'trace'
				stream: process.stdout
			}, {
				level:'trace'
				type: 'raw'
				stream: log_stream
			}
		]
	}

	# Set environment
	log.info "run in environment: #{env}"
	try
		config.setEnvironment env
	catch e
		log.fatal e
		throw e

	# initialize database
	MongoClient.connect config.DB_HOST, (err, db) ->
		if err != null
			log.fatal {host: config.DB_HOST, err: err}, 'could not connect to mongo'
			throw err
		log.info {host: config.DB_HOST}, 'connected to mongo'
		exports.db = {}
		exports.db.db = db
		exports.db.images = db.collection('images')
		# exports.db.logs = db.collection('serverlogs')
		# exports.db.logs.insert log_queue, (err, docs) ->
		# log_stream.write = (obj) -> 
		# 	exports.db.logs.insert obj, (err, docs) ->

	# initialise socket.io
	module.exports.io = io(server)

	# environment
	exports.env = env


