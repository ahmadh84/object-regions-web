socket = io()

segmenter = {
  options: {}
  display: 0
  segments: []
  url: ''
}

image = {
  canvas_segs: $('#segments').get(0)
  ui: $('#ui').get(0)
  scale: 1
}

image.segs_ctx = image.canvas_segs.getContext("2d")
image.ui_ctx = image.ui.getContext("2d")

image.ui.width = image.canvas_segs.width  = $(image.canvas_segs).parent().width()

$('.toggle').click () -> 
  $( this ).siblings().removeClass('active')
  $( this ).addClass('active')
  segmenter.options[$( this ).parent().attr('option')] = $( this ).attr('choice')

$('.buttons :first-child').click()

$('#url').keyup (e) ->
  text = $('#url').val()
  $('#load').addClass('disabled').unbind()
  if text.match(/.+\.(jpg|png|gif|tif)(\?.*|)/g)?[0] == text
    segmenter.url = text
    $('#load').text('url').removeClass('disabled').click loadImage
  else if text.match(/\d{4}_\d{6}/g)?[0] == text
    $('#load').text('PASCAL').removeClass('disabled').click () ->
      socket.emit('load', {'id': $('#url').val()})
  if e.which == 13
    $('#load').click()

loadImage = () ->
  img = $('<img id="image">').attr('src', segmenter.url)
  img.load () ->
    segmenter.height = this.height
    segmenter.width = this.width
    $('#image').replaceWith(img)
    image.scale = image.canvas_segs.width / this.width
    image.ui.height = image.canvas_segs.height = this.height * image.scale
    $('#imagewrap').height(image.canvas_segs.height)
    img.width(image.canvas_segs.width).height(image.canvas_segs.height)
    img.css { position: 'absolute', left: 0, top: 0 }
    image.segs_ctx.drawImage img.get(0), 0, 0, image.canvas_segs.width, image.canvas_segs.height
    $(ui).click getSegmentOnClick
    $(ui).mousemove (event) -> 
      mouse = getMousePos(event, this)
      image.ui.width = image.ui.width
      drawCursor image.ui_ctx, mouse
  return false

$('#load-db').click () ->
  socket.emit('load', {'id': $('#url').val()})

socket.on 'connect', () ->
  wlog 'connected to rigor server\n'

socket.on 'disconnect', () ->
  wlog 'disconnected from rigor server\n'
  setTimeout( (() -> window.location.reload()) , 2000)

socket.on 'status', (workers) ->
  wlog 'worker status:\n'
  wlog workers

socket.on 'load', (url) ->
  segmenter.url = url
  loadImage()

socket.on 'progress', (msg) ->
  wlog msg

socket.on 'warning', (msg) ->
  wlog msg

socket.on 'error', (msg) ->
  wlog msg

socket.on 'answer', (segments) ->
  image.canvas_segs.width  = image.canvas_segs.width
  # wlog JSON.stringify(segments)
  curSeg = 0
  segmenter.segments = segments
  segspan = $('#segments-panel')
  segspan.attr("class", "");
  segspan.empty()
  for i in [0..segmenter.segments.length-1]
    for j in [0..segmenter.segments[i].B.length-1]
      segmenter.segments[i].B[j] = decodeBoundary(segmenter.segments[i].B[j])
    thumb = $("<canvas>").addClass('thumb').attr('index', i).get(0)
    thumb.width = thumb.height = 10
    thumbdiv = $('<div>').addClass('thumbdiv').append( thumb )
    segspan.append( thumbdiv )
    
  # wlog JSON.stringify(segmenter.segments)
  
  $('#segments-panel').slick({
    slidesToShow: 5
    slidesToScroll: 5
    infinite: false
  })
  img = $('#image').get(0)
  for i in [0..segmenter.segments.length-1]
    thumbs = $('.thumb[index=' + i + ']')
    for thumb in thumbs
      thumb.width = ($('#segments-panel').width() - 20*6) / 5 
      scale = thumb.width / segmenter.width
      thumb.height = segmenter.height * scale
      context = thumb.getContext("2d")
      context.globalAlpha = 0.3
      context.drawImage(img,0,0, thumb.width, thumb.height)
      context.globalAlpha = 1
      drawSegments context, segmenter.segments[i], scale
      $(thumb).click drawSegThumb
  # for thumb in $('.thumb[index=' + 0 + ']')
  #   thumb.addClass 'active'
  drawSegments image.segs_ctx, segmenter.segments[0], image.scale

drawSegThumb = () -> 
  segmenter.display = parseInt($(this).attr('index'))
  $('.thumb').removeClass 'active'
  $(this).addClass 'active'
  image.canvas_segs.width  = image.canvas_segs.width
  drawSegments image.segs_ctx, segmenter.segments[segmenter.display % segmenter.segments.length], image.scale

drawNext = () -> 
  segmenter.display++
  image.canvas_segs.width  = image.canvas_segs.width
  drawSegments image.segs_ctx, segmenter.segments[segmenter.display % segmenter.segments.length], image.scale

getMousePos = (event, parent) ->
  offset = $(parent).offset();
  return {
    'x': (event.pageX - offset.left),
    'y': (event.pageY - offset.top)
  }

getSegmentOnClick = (event) ->
  mouse = getMousePos(event, this)
  $(image.ui).unbind()
  x = Math.floor(mouse.x / image.scale)
  y = Math.floor(mouse.y / image.scale)
  wlog("Segment image with seed -> x: " + x + ", y: " + y + '\n')
  socket.emit('run', {'url': segmenter.url, 'x':x + 1, 'y':y + 1, 'options': segmenter.options})

drawCursor = (context, mouse) ->
  context.strokeStyle = 'black'
  context.lineWidth = 0.5;

  context.beginPath()
  context.lineTo mouse.x, 0
  context.lineTo mouse.x, context.canvas.height
  context.closePath()
  context.stroke()

  context.beginPath()
  context.lineTo 0, mouse.y
  context.lineTo context.canvas.width, mouse.y
  context.closePath()
  context.stroke()

drawSegments = (context, segments, scale, stroke, fill) ->
  context.globalCompositeOperation = 'source-over'
  drawPolygons context,
    segments.B.slice(0, segments.N),
    scale, null, 'rgba(100,100,255,0.5)'

  context.globalCompositeOperation = "destination-out"
  drawPolygons context,
    segments.B.slice(segments.N, segments.B.length),
    scale, null, '#000'
  # 05b,

  context.globalCompositeOperation = 'source-over'
  drawPolygons context, segments.B,
    scale, '#006', null

drawPolygons = (context, polygons, scale, stroke, fill) ->
  for polygon in polygons
    context.beginPath()
    for y, i in polygon[0]
      x = polygon[1][i]
      context.lineTo x * scale, y * scale
    context.closePath()
    if stroke
      context.strokeStyle = stroke
      context.stroke()
    if fill
      context.fillStyle = fill
      context.fill()

escapeStrs = {'n':'\n', 'r':'\r', 't':'\t', '\'': '\'', '\"':'\"', '\\':'\\'}

wlog = (msg) ->
  logd = $('#log')
  logd.append msg.toString()
  logd.scrollTop(logd[0].scrollHeight)

base64map = {'A': 0, 'R': 17, 'i': 34, 'z': 51, 'B': 1, 'S': 18, 'j': 35, '0': 52, 'C': 2, 'T': 19, 'k': 36, '1': 53, 'D': 3, 'U': 20, 'l': 37, '2': 54, 'E': 4, 'V': 21, 'm': 38, '3': 55, 'F': 5, 'W': 22, 'n': 39, '4': 56, 'G': 6, 'X': 23, 'o': 40, '5': 57, 'H': 7, 'Y': 24, 'p': 41, '6': 58, 'I': 8, 'Z': 25, 'q': 42, '7': 59, 'J': 9, 'a': 26, 'r': 43, '8': 60, 'K': 10, 'b': 27, 's': 44, '9': 61, 'L': 11, 'c': 28, 't': 45, '-': 62, 'M': 12, 'd': 29, 'u': 46, '_': 63, 'N': 13, 'e': 30, 'v': 47, 'O': 14, 'f': 31, 'w': 48, 'P': 15, 'g': 32, 'x': 49, 'Q': 16, 'h': 33, 'y': 50}
offmap = [0, 1, -1]

char2bin = (c) ->
  bin = base64map[c].toString(2)
  (new Array(7 - bin.length).join('0') + bin)

decodeBoundary = (bStr) ->
  head = bStr.substring(0, 6)
  headbin = (char2bin(c) for c in head).join('')

  curx = parseInt headbin.substring(0, 16), 2
  cury = parseInt headbin.substring(16, 32), 2
  odd = headbin.charAt(32)
  off2 = parseInt(headbin.substring(33, 36), 2) + 1
  ox2 = offmap[Math.floor(off2 / 3)]
  oy2 = offmap[off2 % 3]

  x = [curx]
  y = [cury]

  if odd == '1'
    x.push curx += ox2
    y.push cury += oy2

  for c in  bStr.substring(6, bStr.length)
    cbin = char2bin c
    l = parseInt(cbin.substring(0, 3), 2) + 1
    r = parseInt(cbin.substring(3, 6), 2) + 1
    x1 = offmap[Math.floor(l / 3)]
    y1 = offmap[l % 3]
    x2 = offmap[Math.floor(r / 3)]
    y2 = offmap[r % 3]
    x.push curx += x1
    y.push cury += y1
    x.push curx += x2
    y.push cury += y2
  return [x, y]

socket.on 'css', (file) ->
  console.log 'css file changed: ' + file
  d = new Date();
  link = '/css/' + file + '.css'
  $('link[href^="'+ link + '"]').attr("href", link+'?'+d.getTime());
