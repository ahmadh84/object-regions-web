# Rigor Demo Web App

Demo web app for segmentation algorithms. 

## Requirements

* [NodeJs](http://nodejs.org)
* [Express](http://expressjs.com)
* [CoffeeScript](http://coffeescript.org)
* [Jade](http://jade-lang.com/)
* [Stylus](http://learnboost.github.io/stylus/)
* [bootstrap-stylus](https://github.com/Acquisio/bootstrap-stylus)
* [Nib](http://visionmedia.github.io/nib/)
* [connect-assets](http://github.com/TrevorBurnham/connect-assets)
* [Mocha](http://visionmedia.github.com/mocha/)
* [Mongoose](https://github.com/LearnBoost/mongoose)

These will install with npm, just do 

```
npm install
```

In your project directory (assuming node is installed). 

## Install coffee-script, bunyan and forever globally

``` sh
npm install coffee-script -g
npm install bunyan -g
npm install forever -g
```

## Run

```
cake dev | bunyan
```

## Rigor Worker

Connect it to a rigor worker from [Rigor](http://cpl.cc.gatech.edu/projects/RIGOR/). 

After installing rigor, run the matlab function, 

```
worker(port)
```

in rigor/server, where 'port' is the port the web server is listening for workers (default is 5000).

## Logs

	coffee logs.coffee | bunyan -l info
	
-l specifies the logging level. 

The levels are fatal > error > warn > info > debug > trace