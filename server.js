var app = require('./.app');
var log = require('./.app/app').log;
var port = app.port;

app.listen(port, function() {
  log.info({port: port}, "Web server started and listening")
  console.log("Press CTRL-C to stop server.");
});
